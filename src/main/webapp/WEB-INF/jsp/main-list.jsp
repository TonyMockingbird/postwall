<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>

<head>
    <title>Persons list</title>
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/res/css/person.css"/>
</head>

    <body>
        <div class="container">
            <c:if test="${!empty persons}">
                <c:forEach items="${persons}" var="person">

                        <div class="post-container">
                            <div class="person-container">
                                <c:out value="${person.firstName}"/><br>
                                <c:out value="${person.lastName}"/>
                            </div>
                            <div class="post-text">
                                Bonbon donut brownie biscuit candy canes wafer biscuit. Jelly beans toffee brownie powder icing jelly beans fruitcake apple pie icing. Carrot cake marzipan gummies bear claw cheesecake jujubes unerdwear.com. Halvah jujubes bonbon.
                                Bonbon donut brownie biscuit candy canes wafer biscuit. Jelly beans toffee brownie powder icing jelly beans fruitcake apple pie icing. Carrot cake marzipan gummies bear claw cheesecake jujubes unerdwear.com. Halvah jujubes bonbon.
                                Bonbon donut brownie biscuit candy canes wafer biscuit. Jelly beans toffee brownie powder icing jelly beans fruitcake apple pie icing. Carrot cake marzipan gummies bear claw cheesecake jujubes unerdwear.com. Halvah jujubes bonbon.
                                Bonbon donut brownie biscuit candy canes wafer biscuit. Jelly beans toffee brownie powder icing jelly beans fruitcake apple pie icing. Carrot cake marzipan gummies bear claw cheesecake jujubes unerdwear.com. Halvah jujubes bonbon.

                            </div>
                                <c:if test="${!empty person.posts}">
                                    <div class="comment-container">
                                        <c:forEach items="${person.posts}" var="post">

                                            <div class="comment-text">
                                                <c:out value="${post.postBody}"/>
                                            </div>

                                        </c:forEach>
                                    </div>
                                </c:if>

                                <c:url var="addPostUrl" value="/add?id=${person.id}"/>

                                <form:form modelAttribute="postObject" method="POST" action="${addPostUrl}">
                                    <form:textarea cssClass="add-comment-textarea" path="postBody" placeholder="add comment..."/>

                                    <input class="input-submit-button" type="submit" value="Commented"/>
                                </form:form>
                        </div>
                </c:forEach>
            </c:if>
        </div>
    </body>
</html>
















