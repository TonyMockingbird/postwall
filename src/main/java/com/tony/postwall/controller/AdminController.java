package com.tony.postwall.controller;

import com.tony.postwall.model.dto.PersonDto;
import com.tony.postwall.model.entity.Person;
import com.tony.postwall.model.entity.Post;
import com.tony.postwall.service.PersonService;
import com.tony.postwall.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acer on 29.01.2016.
 */

@Controller
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private PersonService personService;

	@Autowired
	private PostService postService;

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String listPerson(Model model){

		List<Person> persons = personService.listContact();
		List<PersonDto> personsDTO = new ArrayList<PersonDto>();

		for (Person person : persons){
			PersonDto personDTO = new PersonDto();

			personDTO.setId(person.getPersonId());
			personDTO.setFirstName(person.getFirstName());
			personDTO.setLastName(person.getLastName());
			personDTO.setPosts(postService.listPostsByPersonId(person.getPersonId()));

			personsDTO.add(personDTO);
		}

		model.addAttribute("persons", personsDTO);
		//add post to model
		Post post = new Post();
		model.addAttribute("postObject", post);

		return "admin-list";
	}

	@RequestMapping(value="/delete/post", method = RequestMethod.GET)
	public String deletePost(@RequestParam("postId") Integer postId){
		postService.deletePost(postId);

		return "redirect:/admin/home";
	}

	@RequestMapping(value="/edit/post", method = RequestMethod.GET)
	public String editPost(@RequestParam("postId") Integer postId, @RequestParam("personId") Integer perosnId, Model model){

		return "redirect:/admin/home";
	}

}