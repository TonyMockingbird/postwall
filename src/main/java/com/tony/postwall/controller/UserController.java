package com.tony.postwall.controller;

import com.tony.postwall.model.dto.PersonDto;
import com.tony.postwall.model.entity.Person;
import com.tony.postwall.model.entity.Post;
import com.tony.postwall.service.PersonService;
import com.tony.postwall.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acer on 29.01.2016.
 */

@Controller
public class UserController {

	@Autowired
	private PersonService personService;

	@Autowired
	private PostService postService;

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String listPerson(Model model){

		List<Person> persons = personService.listContact();
		List<PersonDto> personsDTO = new ArrayList<PersonDto>();

		for (Person person : persons){

			PersonDto personDTO = new PersonDto();

			personDTO.setId(person.getPersonId());
			personDTO.setFirstName(person.getFirstName());
			personDTO.setLastName(person.getLastName());
			personDTO.setPosts(postService.listPostsByPersonId(person.getPersonId()));

			personsDTO.add(personDTO);
		}

		model.addAttribute("persons", personsDTO);
		//add post to model
		Post post = new Post();
		model.addAttribute("postObject", post);

		return "main-list";
	}

	@RequestMapping(value="/add", method = RequestMethod.POST)
	public String addPost(@RequestParam("id") Integer id, @ModelAttribute("postObject") Post post){
		postService.addPost(id, post);

		return "redirect:/home";
	}

	@RequestMapping(value = "/addPerson", method = RequestMethod.POST)
	public String addPerson(@ModelAttribute("personAttribute") Person person){
		personService.addPerson(person);

		return "redirect:/home";
	}

	@RequestMapping("/delete/{id}")
	public String deletePerson(@PathVariable("id") Integer id){
		personService.removePerson(id);

		return "redirect:/home";
	}
}
