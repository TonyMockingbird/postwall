package com.tony.postwall.model.entity;

import javax.persistence.*;

/**
 * Created by acer on 29.01.2016.
 */

@Entity
@Table(name = "post")
public class Post implements java.io.Serializable{

	@Id
	@GeneratedValue
	@Column(name = "post_id")
	private int postId;

	@Column(name = "post_body")
	private String postBody;

	@ManyToOne
	@JoinColumn(name="fk_person_id")
	private Person person;

	public int getPostId() {
		return postId;
	}

	public void setPostId(int post_id) {
		this.postId = post_id;
	}

	public String getPostBody() {
		return postBody;
	}

	public void setPostBody(String post_body) {
		this.postBody = post_body;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public String toString() {
		return postBody;
	}
}

