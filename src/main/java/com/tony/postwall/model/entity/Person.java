package com.tony.postwall.model.entity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by acer on 29.01.2016.
 */

@Entity
@Table(name = "person")
public class Person implements java.io.Serializable{

	@Id
	@Column(name = "person_id")
	@GeneratedValue
	private int personId;

	@Column(name = "f_name")
	private String firstName;

	@Column(name = "l_name")
	private String lastName;

	@OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
	private List<Post> posts;

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int person_id) {
		this.personId = person_id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String f_name) {
		this.firstName = f_name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String l_name) {
		this.lastName = l_name;
	}
}
