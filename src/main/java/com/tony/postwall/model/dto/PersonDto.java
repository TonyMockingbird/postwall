package com.tony.postwall.model.dto;

import com.tony.postwall.model.entity.Post;

import java.util.List;

/**
 * Created by acer on 29.01.2016.
 */

public class PersonDto {

	private Integer id;
	private String firstName;
	private String lastName;
	private List<Post> posts;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}
}
