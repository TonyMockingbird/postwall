package com.tony.postwall.model.dao.impl;

import com.tony.postwall.model.dao.PostDao;
import com.tony.postwall.model.entity.Person;
import com.tony.postwall.model.entity.Post;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acer on 29.01.2016.
 */

@Repository
public class PostDaoImpl implements PostDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Post> listPostsByPersonId(Integer id) {

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Person as person left join fetch person.posts where person.id="+id);
		Person person = (Person) query.uniqueResult();

		return new ArrayList<Post>(person.getPosts());
	}

	@Override
	public List<Post> listPost() {

		Session session = sessionFactory.getCurrentSession();

		Query query = session.createQuery("from Post");

		return query.list();
	}

	@Override
	public void addPost(Integer id, Post post) {

		Session session = sessionFactory.getCurrentSession();

		Person person = (Person) session.get(Person.class, id);

		post.setPerson(person);
		person.getPosts().add(post);
	}

	@Override
	public void deletePost(Integer postId){

		Session session = sessionFactory.getCurrentSession();
		Post post = (Post) session.get(Post.class, postId);
		session.delete(post);

	}
}
