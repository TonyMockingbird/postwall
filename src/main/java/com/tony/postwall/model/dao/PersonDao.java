package com.tony.postwall.model.dao;

import com.tony.postwall.model.entity.Person;

import java.util.List;

/**
 * Created by acer on 29.01.2016.
 */
public interface PersonDao {

	public void addPerson(Person person);

	public List<Person> listPersons();

	public void removePerson(Integer id);

	public Person findPersonById(Integer id);
}
