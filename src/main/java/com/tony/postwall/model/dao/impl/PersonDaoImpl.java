package com.tony.postwall.model.dao.impl;

import com.tony.postwall.model.dao.PersonDao;
import com.tony.postwall.model.entity.Person;
import com.tony.postwall.model.entity.Post;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by acer on 29.01.2016.
 */

@Repository
public class PersonDaoImpl implements PersonDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addPerson(Person person) {
		sessionFactory.getCurrentSession().save(person);
	}

	@Override
	public List<Person> listPersons() {
		List<Person> persons = sessionFactory.getCurrentSession().createQuery("from Person ").list();
		return persons;
	}

	@Override
	public void removePerson(Integer id) {

		Session session = sessionFactory.getCurrentSession();

		Query query = session.createQuery("from Person as person left join fetch person.posts where person.id =" +id);
		Person person  = (Person) query.uniqueResult();

		List<Post> posts = person.getPosts();

		session.delete(person);

		for (Post post : posts){
			session.delete(post);
		}
	}

	@Override
	public Person findPersonById(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("select Person as person left join fetch person.posts where person.id ="+id);
		Person person = (Person) query.uniqueResult();

		return person;
	}
}
