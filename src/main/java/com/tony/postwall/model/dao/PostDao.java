package com.tony.postwall.model.dao;

import com.tony.postwall.model.entity.Post;

import java.util.List;

/**
 * Created by acer on 29.01.2016.
 */
public interface PostDao {

	public List<Post> listPostsByPersonId(Integer id);

	public List<Post> listPost();

	public void addPost(Integer id, Post post);

	public void deletePost(Integer postId);
}
