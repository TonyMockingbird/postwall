package com.tony.postwall.service.impl;


import com.tony.postwall.model.dao.PostDao;
import com.tony.postwall.model.entity.Post;
import com.tony.postwall.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by acer on 29.01.2016.
 */

@Service
@Transactional
public class PostServiceImpl implements PostService {

    @Autowired
    private PostDao postDAO;

    @Override
    public List<Post> listPostsByPersonId(Integer id) {

        List<Post> posts = postDAO.listPostsByPersonId(id);
        return posts;
    }

    @Override
    public List<Post> listPost() {
        return postDAO.listPost();
    }

    @Override
    public void addPost(Integer id, Post post) {
        postDAO.addPost(id, post);
    }

    @Override
    public void deletePost(Integer postId) {
        postDAO.deletePost(postId);
    }


}
