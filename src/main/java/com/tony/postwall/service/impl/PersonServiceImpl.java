package com.tony.postwall.service.impl;

import com.tony.postwall.model.dao.PersonDao;
import com.tony.postwall.model.entity.Person;
import com.tony.postwall.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by acer on 29.01.2016.
 */

@Service
@Transactional
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonDao personDAO;

    @Override
    public void addPerson(Person person) {
        personDAO.addPerson(person);
    }

    @Override
    public List<Person> listContact() {
        return personDAO.listPersons();
    }

    @Override
    public void removePerson(Integer id) {
        personDAO.removePerson(id);
    }

    @Override
    public Person findPersonById(Integer id) {
        return personDAO.findPersonById(id);
    }
}
